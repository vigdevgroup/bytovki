$(function () {

    new WOW().init();

    $('.portfolio-main__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: '<button class="slick-prev"><img src="img/pmain-prev.png" ></button>',
        nextArrow: '<button class="slick-next"><img src="img/pmain-next.png" ></button>',
        responsive: [
            {
                breakpoint: 926,
                settings: {
                    prevArrow: '<button class="slick-prev"><img src="img/pmain-prev-big.png" ></button>',
                    nextArrow: '<button class="slick-next"><img src="img/pmain-next-big.png" ></button>',
                }
            }
        ]
    });

    $('.portfolio-common').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: '<button class="slick-prev"><img src="img/pcom-prev.png" ></button>',
        nextArrow: '<button class="slick-next"><img src="img/pcom-next.png" ></button>',
        responsive: [
            {
                breakpoint: 1231,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 926,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    prevArrow: '<button class="slick-prev"><img src="img/pmain-prev-big.png" ></button>',
                    nextArrow: '<button class="slick-next"><img src="img/pmain-next-big.png" ></button>',
                }
            }
        ]
    });

    $('.portfolio-common__item-inner').each(function () {
        $(this).magnificPopup({
            type: 'image',
            tLoading: '',
            removalDelay: 300,
            mainClass: 'mfp-img-mobile mfp-fade',
            fixedContentPos: true,
        })
    });

    //Mobile menu

    document.addEventListener('click', menu);

    // Navigation

    document.addEventListener('click', navigation);

    // Header

    document.addEventListener('scroll', headerPos);
    let headerH = document.querySelector('.header').offsetHeight;

    //Modal

    document.addEventListener('click', function () {

        if (!event.target.closest('.btn-modal')) return;

        event.preventDefault();

        let modal = document.querySelector('#default-modal');
        let content = document.querySelector('.form-module').innerHTML;

        openModal(modal, content);
    });

    //Sale modal

    function saleModal() {
        if (document.querySelector('#default-modal').style.display != 'flex' && !(document.body.classList.contains('mfp-zoom-out-cur'))) {
            let modal = document.querySelector('#sale-modal');
            openModal(modal, null);
        } else {
            setTimeout(saleModal, 30000);
        }
    }

    setTimeout(saleModal, 30000);

    //Forms and inputs

    document.addEventListener('change', function () {
        checkInput(event.target);
    });
    document.addEventListener('submit', checkForm);
    $(document).on('change', function () {
        let target = event.target;

        if (!target.closest('.checkbox-wrapper')) return;

        let box = target.closest('.checkbox-wrapper');

        let checkbox = box.querySelector('input');

        if (checkbox.checked) {
            box.querySelector('.checkbox').classList.add('checked');
        } else {
            box.querySelector('.checkbox').classList.remove('checked');
        }
    });

    let errs = {};
    document.querySelectorAll('form').forEach((form) => {
        errs[form] = [];
    });

    //Mask for phone

    $('.contacts input[type="tel"]').each(function () {
        $(this).mask('+7 (000) 000 00 00', {
            onChange: function (cep, e) {
                checkPhone(cep, e);
            }
        });
    });

    //Timer

    let deadline = new Date(document.querySelector('#timer').dataset.date || '');
    let timeinterval = null
    if (deadline > new Date()) {
        initializeClock("timer", deadline);
    }

    //Portfolio

    document.addEventListener('click', portfolioTabs);
    let currentTab = document.querySelector('.list-btn.active');

    //Resize

    window.addEventListener('resize', checkWidth);
    checkWidth();

    //Advan

    let curAdvan = null;

    //Functions

    function advanAccord() {

        if (!(event.target.closest('.advan__item') && !event.target.closest('.advan__item-text'))) return;
        let target = event.target.closest('.advan__item');

        if (curAdvan) {
            if (curAdvan == target) {
                let text = curAdvan.querySelector('.advan__item-text');
                curAdvan.classList.remove('active');
                $(text).slideUp(300);
                curAdvan = null;
            } else {
                let text = curAdvan.querySelector('.advan__item-text');
                curAdvan.classList.remove('active');
                $(text).slideUp(300);
                curAdvan = target;
                text = curAdvan.querySelector('.advan__item-text');
                curAdvan.classList.add('active');
                $(text).slideDown(300);
            }
        } else {
            curAdvan = target;
            let text = curAdvan.querySelector('.advan__item-text');
            curAdvan.classList.add('active');
            $(text).slideDown(300);
        }
    }

    function pmainSelect(event) {

        if (!event.target.closest('.portfolio-main__select')) return;

        event.target.classList.add('active');

        $('.portfolio-main__list').slideDown(300);

        document.addEventListener('click', closeSelect);
    }

    function closeSelect(event) {

        let target = event.target;

        if (target.closest('.list-btn') || target.closest('.portfolio-main__select') || !target.closest('.portfolio-main__list-wrapper')) {
            document.querySelector('.portfolio-main__select').classList.remove('active');
            $('.portfolio-main__list').slideUp(300);
            document.removeEventListener('click', closeSelect);
        }
    }

    function checkWidth() {
        if (document.documentElement.clientWidth <= 1145) {
            document.addEventListener('click', pmainSelect);
        } else {
            document.removeEventListener('click', pmainSelect);
        }
        if (document.documentElement.clientWidth <= 500) {
            document.addEventListener('click', advanAccord);
        } else {
            document.removeEventListener('click', advanAccord);
        }
        if (document.querySelector('.header__mobile').style.display == 'block') {
            document.querySelector('.header__mobile').style.maxHeight = document.documentElement.clientHeight + 'px';
        }
    }

    function menu() {
        if (!event.target.closest('.header__menu-btn')) return;

        let menu = document.querySelector('.header__menu').innerHTML;

        document.querySelector('.header__mobile-menu').innerHTML = menu;
        document.querySelector('.header__mobile').style.maxHeight = document.documentElement.clientHeight + 'px';
        $('.header__mobile').slideDown(500);

        document.addEventListener('click', function () {
            closeMenu(event.target);
        });
    }

    function closeMenu(target) {

        if (!target.classList.contains('close')) return;
        if (!target.closest('.header__mobile')) return;

        $('.header__mobile').slideUp(500);

        document.removeEventListener('click', closeMenu);
    }

    function headerPos() {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        let header = document.querySelector('.header');
        let container = document.querySelector('.document-container');

        if (scrollTop > headerH + 50) {
            header.classList.remove('off');
            container.style.paddingTop = headerH + 'px';
            header.classList.add('fixed');
            setTimeout(() => {
                header.classList.add('on');
            }, 100);
        } else {
            header.classList.remove('on');
            header.classList.remove('fixed');
            container.style.paddingTop = '';
        }
    }

    function portfolioTabs(event) {
        if (!event.target.closest('.list-btn')) return;

        if (event.target.classList.contains('active')) return;

        let target = event.target;
        currentTab.classList.remove('active');
        currentTab = target;
        currentTab.classList.add('active');

        let select = document.querySelector('.portfolio-main__select');

        if (getComputedStyle(select).display == 'block') {
            select.querySelector('span').innerHTML = currentTab.innerHTML;
        }

        let dataTarget = document.querySelector('.portfolio-' + target.dataset.target);
        let price = dataTarget.querySelector('.portfolio-module__price').innerHTML;
        let area = dataTarget.querySelector('.portfolio-module__area').innerHTML;
        let gallery = dataTarget.querySelector('.portfolio-module__gallery').innerHTML;
        let portfolio = target.closest('.portfolio-main');
        let slider = portfolio.querySelector('.portfolio-main__slider');

        portfolio.querySelector('.portfolio-main__price-value').innerHTML = price;
        portfolio.querySelector('.portfolio-main__price-area').innerHTML = area;

        $(slider).slick('unslick');
        slider.innerHTML = gallery;
        $(slider).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            prevArrow: '<button class="slick-prev"><img src="img/pmain-prev.png" ></button>',
            nextArrow: '<button class="slick-next"><img src="img/pmain-next.png" ></button>',
            responsive: [
                {
                    breakpoint: 926,
                    settings: {
                        prevArrow: '<button class="slick-prev"><img src="img/pmain-prev-big.png" ></button>',
                        nextArrow: '<button class="slick-next"><img src="img/pmain-next-big.png" ></button>',
                    }
                }
            ]
        });
    }

    function openModal(modal, content) {

        if (content) modal.querySelector('.modal__body').innerHTML = content;

        $(modal).find('input[type="tel"]').each(function () {
            $(this).mask('+7 (000) 000 00 00', {
                onChange: function (cep, e) {
                    checkPhone(cep, e);
                }
            });
        });

        modal.style.display = 'flex';
        modal.classList.remove('off');
        modal.classList.add('on');

        modal.addEventListener('click', function () {
            closeModal(event, modal);
        });
        document.addEventListener('keyup', function () {
            closeModal(event, modal);
        });

    }

    function closeModal(event, modal) {

        let target = event.target;

        if (!(target.closest('.close') || target.classList.contains('modal') || event.key == 'Escape')) return;

        modal.classList.remove('on');
        modal.classList.add('off');
        setTimeout(() => {
            modal.style.display = 'none';
        }, 1500);
        modal.removeEventListener('click', closeModal);
        document.removeEventListener('keyup', closeModal);
    }

    function navigation() {
        let target = event.target;

        if (target.tagName != 'A') return;
        if (!target.closest('.menu')) return;

        event.preventDefault();

        if (document.querySelector('.header__mobile').style.display == 'block') {
            closeMenu(document.querySelector('.header__mobile').querySelector('.close'));
        }

        let obj;
        let top;

        if (target.dataset.target == 'top') {
            top = 0;
        } else {
            obj = document.querySelector('.' + target.dataset.target + '__title');
            top = $(obj).offset().top - 150;
        }

        $('html, body').animate({
            scrollTop: top
        }, 800);
    }

    function checkInput(target) {
        let type = target.type;
        let form = target.closest('form');
        let regexp;
        let index = errs[form].indexOf(target);

        switch (type) {
            case 'text':
                regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
                if (!target.value) {
                    form.nameValid = false;
                    target.closest('.input-wrapper').classList.remove('success');
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[form].splice(index, 1);
                    }
                }
                else if (regexp.test(target.value)) {
                    form.nameValid = true;
                    target.closest('.input-wrapper').classList.add('success');
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[form].splice(index, 1);
                    }
                }
                else {
                    form.nameValid = false;
                    target.closest('.input-wrapper').classList.remove('success');
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[form].includes(target)) {
                        errs[form].push(target);
                    }
                }
                break;
            case 'checkbox':
                if (target.checked) {
                    if (index != -1) {
                        errs[form].splice(index, 1);
                        target.closest('.checkbox-wrapper').classList.remove('error');
                    }
                }
                else {
                    if (!errs[form].includes(target)) {
                        errs[form].push(target);
                        target.closest('.checkbox-wrapper').classList.add('error');
                    }
                }
                break;
        }
    }

    function checkPhone(value, event) {
        let target = event.target;
        let form = target.closest('form');
        let index = errs[form].indexOf(target);

        if (target.value.length == 0) {
            form.telValid = false;
            target.closest('.input-wrapper').classList.remove('success');
            target.closest('.input-wrapper').classList.remove('error');
            if (index != -1) {
                errs[form].splice(index, 1);
            }
        }
        else if (target.value.length == 18) {
            form.phoneValid = true;
            target.closest('.input-wrapper').classList.add('success');
            target.closest('.input-wrapper').classList.remove('error');
            if (index != -1) {
                errs[form].splice(index, 1);
            }
        }
        else {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('success');
            target.closest('.input-wrapper').classList.add('error');
            if (!errs[form].includes(target)) {
                errs[form].push(target);
            }
        }
    }

    function checkForm() {
        event.preventDefault();

        let form = event.target;

        let data = new FormData();
        if (form.name) {
            if (!(form.nameValid) && !(errs[form].includes(form.name))) {
                errs[form].push(form.name);
            } else {
                data.append('name', form.name.value);
            }
        }
        if (form.phone) {
            if (!(form.phoneValid) && !(errs[form].includes(form.phone))) {
                errs[form].push(form.phone);
            } else {
                data.append('phone', form.phone.value);
            }
        }
        if (errs[form].length != 0) {
            for (err of errs[form]) {
                if (err.name != 'confirm') {
                    err.closest('.input-wrapper').classList.remove('success');
                    err.closest('.input-wrapper').classList.add('error');
                }
            }
            return;
        }

        if (!(form.confirm.checked) && !(errs[form].includes(form.confirm))) {
            errs[form].push(form.confirm);
            form.confirm.closest('.checkbox-wrapper').classList.add('error');
            return;
        }

        if (form.closest('.modal')) {
            let modalBody = form.closest('.modal').querySelector('.modal__body');
            modalBody.classList.add('off');

            setTimeout(() => {
                modalBody.innerHTML = document.querySelector('.thanks-module').innerHTML;
                setTimeout(() => {
                    modalBody.classList.remove('off');
                }, 500);
            }, 500);

        } else {
            let modal = document.querySelector('#default-modal');
            let content = document.querySelector('.thanks-module').innerHTML;
            openModal(modal, content);
            clearForm(form);
        }

    }

    function clearForm(form) {
        if (form.name) {
            form.name.value = '';
            form.name.closest('.input-wrapper').classList.remove('success');
            form.nameValid = false;
        }
        if (form.phone) {
            form.phone.value = '';
            form.phone.closest('.input-wrapper').classList.remove('success');
            form.phoneValid = false;
        }
    }

    function getTimeRemaining(endtime) {
        let t = Date.parse(endtime) - Date.parse(new Date());
        let seconds = Math.floor((t / 1000) % 60);
        let minutes = Math.floor((t / 1000 / 60) % 60);
        let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        let days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            total: t,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds
        };
    }

    function initializeClock(id, endtime) {
        let clock = document.getElementById(id);
        let daysSpan = clock.querySelector(".days > .value");
        let hoursSpan = clock.querySelector(".hours > .value");
        let minutesSpan = clock.querySelector(".minutes > .value");
        let secondsSpan = clock.querySelector(".seconds > .value");

        function updateClock() {
            let t = getTimeRemaining(endtime);

            if (t.total <= 0) {
                if (timeinterval) clearInterval(timeinterval);
                daysSpan.innerHTML = '00';
                hoursSpan.innerHTML = '00';
                minutesSpan.innerHTML = '00';
                secondsSpan.innerHTML = '00';
            }

            daysSpan.innerHTML = ("0" + t.days).slice(-2);
            hoursSpan.innerHTML = ("0" + t.hours).slice(-2);
            minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);
            secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);
        }

        updateClock();
        timeinterval = setInterval(updateClock, 1000);
    }

});